package com.example.stickerapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/*
 *  Author   Ali Tamoor
 *  Github   https://github.com/alitamoor65
 *  Date     2019.
 *
 */

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
}
